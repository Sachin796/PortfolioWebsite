require("dotenv").config();

module.exports = {
  development: {
    username: 'root',
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    host: process.env.HOST,
    port: 5432
  },
  production: {
    username: 'root',
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    host: process.env.HOST,
    port: 5432
  },
};
