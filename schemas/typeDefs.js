const typeDefs = `
    type skillsDetail {
        id: ID
        caption: String
        link: String
    }

    type Query {
        getFrontendData: [skillsDetail]
        getBackendData: [skillsDetail]
        getDatabaseSkills: [skillsDetail]
        getLanguageSkills: [skillsDetail]
    }
`

module.exports = typeDefs;
