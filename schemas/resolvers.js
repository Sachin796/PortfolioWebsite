const db = require('../model/index')

const resolvers = {
    Query: {
        getFrontendData: async () =>{
            return await db.frontend.findAll({})
        },
        getBackendData: async () =>{
            return await db.backend.findAll({})
        },
        getDatabaseSkills: async () =>{
            return await db.database.findAll({})
        },
        getLanguageSkills: async () =>{
            return await db.programming.findAll({})
        }
    }
}

module.exports = resolvers;
