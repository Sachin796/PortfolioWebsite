/* eslint-disable no-redeclare */
"use strict";

var fs = require("fs");
var path = require("path");
var { Sequelize, DataTypes } = require("sequelize");
var basename = path.basename(module.filename);
var env = process.env.NODE_ENV || "development";
var config = require(`${__dirname}/../config/config.js`)[env];
var db = {};
let { username, database, password } = config
const sequelizeConnection = new Sequelize(database, username, password,{
  dialect:'postgres',
  ssl: false,
  pool: {
    max: 5,
    idle: 30000,
    acquire: 60000,
  }
})
fs.readdirSync(__dirname)
  .filter(function(file) {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach(function(file) {

    var model = require(path.join(__dirname, file))(sequelizeConnection, DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelizeConnection = sequelizeConnection;
db.Sequelize = Sequelize;

module.exports = db;
