const FrontEnd = require('../model/frontendmodel.js');

const frontEndSeedData = [
    {
        caption: 'HTML',
        link: './images/html-5.svg'
    },
    {
        caption: 'CSS',
        link: './images/css.svg'
    }, 
    {
        caption: 'JAVASCRIPT',
        link: './images/java-script.svg'
    },
    {
        caption: 'BOOTSTRAP',
        link: './images/bootstrap.svg'
    },
    {
        caption: 'REACT',
        link: './images/react.svg'
    }, 
    {
        caption: 'SASS',
        link: './images/sass.svg'
    }
];

const seedFrontend = (sequelize, DataTypes) => FrontEnd(sequelize, DataTypes).bulkCreate(frontEndSeedData);

module.exports = seedFrontend;
