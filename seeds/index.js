const db = require("../model");
var { DataTypes } = require("sequelize");
const seedBackend = require('./backendData');
const seedDatabase = require('./databaseData');
const seedFrontend = require('./frontendData');
const seedLanguages = require('./languageData');

const seedAll = async () => {

  await db.sequelizeConnection.sync({ force: true });

  await seedBackend(db.sequelizeConnection, DataTypes);
  await seedDatabase(db.sequelizeConnection, DataTypes);
  await seedFrontend(db.sequelizeConnection, DataTypes);
  await seedLanguages(db.sequelizeConnection, DataTypes);

  process.exit(0);

};

seedAll();
