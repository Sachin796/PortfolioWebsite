const backEnd = require('../model/backendmodel.js');

const backEndSeedData = [
    {
        caption: 'NODE',
        link: './images/nodejs.svg'
    },
    {
        caption: 'EXPRESS',
        link: './images/expressjs.svg'
    }, 
    {
        caption: 'DJANGO',
        link: './images/django.svg'
    },
    {
        caption: 'GRAPHQL',
        link: './images/graphql.svg'
    },
];

const seedBackend = (sequelize, DataTypes) => backEnd(sequelize, DataTypes).bulkCreate(backEndSeedData);

module.exports = seedBackend;
