const Database = require('../model/databasemodel.js');

const databaseSeedData = [
    {
        caption: 'POSTGRESQL',
        link: './images/postgres.svg'
    },
    {
        caption: 'MYSQL',
        link: './images/mysql.svg'
    }, 
    {
        caption: 'MONGO DB',
        link: './images/mongodb.svg'
    }
];

const seedDatabase = (sequelize, DataTypes) => Database(sequelize, DataTypes).bulkCreate(databaseSeedData);

module.exports = seedDatabase;
