const Languages = require('../model/languagesmodel.js');

const languageSeedData = [
    {
        caption: 'JAVA',
        link: './images/java.svg'
    },
    {
        caption: 'PHP',
        link: './images/php.svg'
    }, 
    {
        caption: 'PYTHON',
        link: './images/python.svg'
    }, 
    {
        caption: 'RUBY',
        link: './images/ruby.svg'
    },
];

const seedLanguages = (sequelize, DataTypes) => Languages(sequelize, DataTypes).bulkCreate(languageSeedData);

module.exports = seedLanguages;
