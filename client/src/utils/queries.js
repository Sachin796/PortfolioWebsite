import { gql } from '@apollo/client';

export const GET_FRONTEND_SKILLS = gql`
    query getFrontendData {
        getFrontendData {
            caption
            id
            link
          }
    }
`
export const GET_BACKEND_SKILLS = gql`
    query getBackendData {
        getBackendData {
            caption
            id
            link
          }
    }
`
export const GET_DATABASE_SKILLS = gql`
    query getDatabaseSkills {
        getDatabaseSkills {
            caption
            id
            link
          }
    }
`
export const GET_LANGUAGE_SKILLS = gql`
    query getLanguageSkills {
        getLanguageSkills {
            caption
            id
            link
          }
    }
`
