import React from "react";
import Links from "../socialiconlinks";

const Intro = () => {
  return (
    <div className="row" id="home">
      <div id="introDiv" className="center-align">
        <section>
          {/* TODO Decide on what should be the header of the file */}
          <h2 id="name">
          </h2>
        </section>
        <section id="description">
          <h6>A Full Stack Web Developer focusing on building </h6>
          <h6>efficient,reliable web application with </h6>
          <h6>great user experience .</h6>
        </section>
        <Links></Links>
      </div>
    </div>
  );
}

export default Intro;
