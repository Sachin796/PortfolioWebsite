import React, { useState, useEffect } from "react";
import TechTemplate from "../technologiesTemplate/index";
import { useQuery } from "@apollo/client";
import { GET_FRONTEND_SKILLS } from '../../utils/queries'

const FrontEnd = () => {

  const [frontEndData, setFrontEndData] = useState(null);
  const {loading, error, data} = useQuery(GET_FRONTEND_SKILLS)

  useEffect(()=>{
    if(error){
      console.error('Error fetching frontend data:', error);
    }
    if(!loading && data){
      setFrontEndData(data.getFrontendData)
    }
  },[loading, data]) 

    return (
      <>
        {frontEndData
          ? frontEndData.map((data) => (
                <TechTemplate
                  id={data.caption}
                  className="col l2 m12 s12"
                  src={data.link}
                  figcaption={data.caption}
                ></TechTemplate>
              )
            )
          : null}
      </>
    );
}

export default FrontEnd;
