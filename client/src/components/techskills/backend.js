import React, { useState, useEffect } from "react";
import TechTemplate from "../technologiesTemplate/index";
import { useQuery } from "@apollo/client";
import { GET_BACKEND_SKILLS } from '../../utils/queries'

const BackEnd = () => {
  const [backendData, setBackendData] = useState(null);
  const {loading, error, data} = useQuery(GET_BACKEND_SKILLS)

  useEffect(()=>{
    if(error){
      console.error('Error fetching backend data:', error);
    }
    if(!loading && data){
      setBackendData(data.getBackendData)
    }
  },[loading, data]) 

    return (
      <>
        {backendData
          ? backendData.map((data) =>(
                <TechTemplate
                  id={data.caption}
                  className="col l2 m12 s12"
                  src={data.link}
                  figcaption={data.caption}
                ></TechTemplate>
              )
            )
          : null}
      </>
    );

}

export default BackEnd;
