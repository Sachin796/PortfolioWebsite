import React, { useState, useEffect } from "react";
import TechTemplate from "../technologiesTemplate/index";
import { useQuery } from "@apollo/client";
import { GET_DATABASE_SKILLS } from '../../utils/queries'

const Database = () => {
  const [databaseData, setDatabaseData] = useState(null);
  const {loading, error, data} = useQuery(GET_DATABASE_SKILLS)

  useEffect(()=>{
    if(error){
      console.error('Error fetching database data:', error);
    }
    if(!loading && data){
      setDatabaseData(data.getDatabaseSkills)
    }
  },[loading, data]) 

    return (
      <>
        {databaseData
          ? databaseData.map((data) => (
                <TechTemplate
                  id={data.caption}
                  className="col l2 m12 s12"
                  src={data.link}
                  figcaption={data.caption}
                ></TechTemplate>
              )
            )
          : null}
      </>
    );
}

export default Database;
