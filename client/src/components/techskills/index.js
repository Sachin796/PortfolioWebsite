import React from "react";
import FrontEnd from "./frontend";
import BackEnd from "./backend";
import Database from "./database";
import Programming from "./programming";

const Techskills = () => {

    return (
      <>
        <div
          className="row"
          style={{
            backgroundColor: "rgb(151, 151, 151,0.3)",
            height: "auto",
          }}
        >
          <div className="row center col l12 m12">
            <h3 id="techskills">
              TECH SKILLS <hr style={{ width: "10%" }} />
            </h3>
            <div style={{ width: "100%" }}>
              <h5>FRONTEND TECHNOLOGIES</h5>
              <div className="" style={{ height: "auto" }}>
                <FrontEnd />
              </div>
            </div>
            <div>
              <h5 className="col l12 m12 s12">BACKEND TECHNOLOGIES</h5>
              <div className="" style={{ height: "auto" }}>
                <BackEnd />
                <fig className="col l4 m12 s12"></fig>
              </div>
            </div>
            <div>
              <h5 className="col l12 m12 s12">DATABASE</h5>
              <div className="col l12 m12 s12" style={{ height: "auto" }}>
                <fig className="col l2 m12 s12"></fig>
                <Database />
                <fig className="col l4 m12 s12"></fig>
              </div>
            </div>
            <div>
              <h5 className="col l12 m12 s12">PROGRAMMING LANGUAGES</h5>
              <div className="col l12 m12 s12" style={{ height: "auto" }}>
                {/*  */}
                <fig className="col l4 m12 s12"></fig>
                <Programming />
                <fig className="col l4 m12 s12"></fig>
              </div>
            </div>
          </div>
        </div>
      </>
    );
}

export default Techskills;
