import React from "react";
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import { Outlet } from 'react-router-dom';
import MainPage from "./pages/index.js";
// import ReactGA from "react-ga";
// require("dotenv").config();
const client = new ApolloClient({
  uri: '/graphql',
  cache: new InMemoryCache(),
});

const App = () => {
    return (
      <ApolloProvider client={client}>
        <MainPage></MainPage>
      </ApolloProvider>
    )
}

export default App;
