import React from "react";
import Intro from "../components/intro";
import NavItems from "../components/nav";
import Footer from "../components/footer";
import AboutMe from "../components/aboutme";
import Contact from "../components/contact";
import Projects from "../components/projects";
import Techskills from "../components/techskills";
import { BrowserRouter as Router } from "react-router-dom";
// require("dotenv").config();

const MainPage = () => {

    return (
      <>
        <Router>
          <NavItems></NavItems>
          <Intro></Intro>
          <AboutMe></AboutMe>
          <Techskills></Techskills>
          <Projects></Projects>
          <Contact></Contact>
          <Footer></Footer>
        </Router>
      </>
    );
}

export default MainPage;
