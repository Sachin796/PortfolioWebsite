const db = require("./model");
const cors = require("cors");
const express = require("express");
const PORT = process.env.PORT || 3001;
// const routes = require("./routes");


const { ApolloServer } = require("@apollo/server");
const { expressMiddleware } = require("@apollo/server/express4");
const path = require("path");
const { typeDefs, resolvers } = require("./schemas");
let syncOptions = {};

const app = express();
const server = new ApolloServer({
  typeDefs,
  resolvers,
});

const startApolloServer = async () => {
  await server.start();

  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());


  app.use(express.static(__dirname + "/public"));
  app.use("/graphql", expressMiddleware(server));
  app.use(cors());

  syncOptions.force = process.env.SYNC_MODEL === "true" ? true : false;
  db.sequelizeConnection.sync(syncOptions).then(() => {
    app.listen(PORT, function () {
      console.log(`🌎 ==> API server now on port ${PORT}!`);
      console.log(`Use GraphQL at http://localhost:${PORT}/graphql`);
    });
  });
};

startApolloServer();
